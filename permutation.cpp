#include <iostream>
#include <list>
#include <string>
using namespace std;

list<string> GetPerms(string ip) {
  if (ip.size() == 1) {
    list<string> perms(1, ip);
    return perms;
  }

  char firstchar = ip[0];
  string remaining = ip.substr(1);
  //cout <<"Remaining: " << remaining << "\n";

  list<string> subperms = GetPerms(remaining);

  //cout << "subperms: ";
  for (string s : subperms) {
    //cout << s << "    ";
  }
  //cout << "\n";
  list<string> perms;

  for (string s : subperms) {
    //cout << "adding to: " << s << "\n";
    for (int i=0; i<=s.size(); i++) {
      string aPerm = s.substr(0, i) + firstchar + s.substr(i);
      //cout << aPerm << "  ";
      perms.push_back(aPerm);
    }
  }

  return perms;
}

int main() {

  list<string> perms = GetPerms("prabud");
  for (string s : perms) {
    cout << s << "\n";
  }

  return 0;
}
