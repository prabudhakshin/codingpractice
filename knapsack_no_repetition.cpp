#include <iostream>
#include <vector>

using namespace std;

vector<int> knapsack(const vector<int> &weights,
                     const vector<int> &values,
                     int C, // capacity
                     int &maxgain // max obtainable value
                     ) {

  int numitems = weights.size();
  // Initialize a pair<int, int> 2D array
  // First - value obtained
  // Second - Whether the item is chosen or not

  pair<int ,bool> grid[C+1][numitems+1];

  // Initialize the first row and first column
  for (int i=0; i<=numitems; i++) 
    grid[0][i] = pair<int, int> (0, false);

  for (int i=0; i<=C; i++) 
    grid[i][0] = pair<int, int> (0, false);

  // fill the rest of the grid
  for (int capacity=1; capacity <= C; capacity++) {
    for (int item=1; item <= numitems; item++) {
      int maxvalue = 0;
      bool itemincluded = false;
      int tempvalue = 0;
      // case where item is not considered
      maxvalue = grid[capacity][item-1].first;

      // case where the item is considered
      if (weights[item-1] <= capacity) {
        tempvalue = grid[capacity-weights[item-1]][item-1].first + values[item-1];
        if (tempvalue > maxvalue) {
          maxvalue = tempvalue;
          itemincluded = true;
        }
      } // else can not include the item anyway

      grid[capacity][item] = pair<int, bool> (maxvalue, itemincluded);
    }
  }

  maxgain = grid[C][numitems].first;

  // fill the index of the items included
  vector<int> included_items;

  for (int capacity=C, item=numitems; capacity!=0 && item!=0;) {
    if (grid[capacity][item].second) {
      // item included
      included_items.push_back(item-1);
      capacity = capacity - weights[item-1];
    }
    item = item-1;
  }

  return included_items;
}

int main() {

  // Input
  vector<int> weights = {3 ,8, 13, 15};
  vector<int> values = {50, 23, 10, 7};
  int C = 25 ;
  ////////

  int maxvalue = 0;
  vector<int> items_included = knapsack(weights, values, C, maxvalue);

  cout << "Max value obtained: " << maxvalue << "\n";
  cout << "Items chosen: \n";
  for (int item : items_included) {
    cout << item << "\n";
  }

  return 0;
}
