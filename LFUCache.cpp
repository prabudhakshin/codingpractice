#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class LFUCache {
  private:
    struct valueobj {
      string key;
      int count;
      string value;
    };

    unordered_map<string, struct valueobj*> hashtable;
    vector<struct valueobj*> min_heap;
    int remaining_capacity;

    static bool comp(struct valueobj* val1, struct valueobj* val2);

  public:
    LFUCache(int capacity);
    string Get(string key);
    void Put(string key, string value);
};

bool LFUCache::comp(struct valueobj* val1, struct valueobj* val2) {
  if (val1->count > val2->count) 
    return true;

  return false;
}

LFUCache::LFUCache(int capacity) {
  remaining_capacity = capacity;
}

string LFUCache::Get(string key) {
  if (hashtable.find(key) == hashtable.end()) {
    cout << "Key not found in the cache; Returning empty value\n";
    return "";
  }

  struct valueobj* val = hashtable.find(key)->second;
  val->count = (val->count)+1;
  make_heap(min_heap.begin(), min_heap.end(), LFUCache::comp);
  return val->value;
}

void LFUCache::Put(string key, string value) {
  if (remaining_capacity == 0 ) {
    // remove the least frequently used item
    pop_heap(min_heap.begin(), min_heap.end(), LFUCache::comp);
    valueobj* val = min_heap.back();
    min_heap.pop_back();
    hashtable.erase(hashtable.find(val->key));
    cout << "Cache full. Evicting '" << val->key << "'\n";
    delete val;
    remaining_capacity++;
  }

  struct valueobj* val = new struct valueobj;
  val->key = key;
  val->value = value;
  val->count = 1;

  hashtable.insert(pair<string, valueobj*>(key, val));
  // add to the heap
  min_heap.push_back(val);
  push_heap(min_heap.begin(), min_heap.end(), LFUCache::comp);
  remaining_capacity--;
}

void getKey(string key, LFUCache& cache) {
  cout << "Value for '" << key << "': ";
  cout << cache.Get(key) << "\n";
}

int main() {
  LFUCache cache(5);
  cache.Put("A", "This is A");
  getKey("A", cache);
  getKey("A", cache);
  cache.Put("B", "This is B");
  cache.Put("C", "This is C");
  cache.Put("D", "This is D");
  getKey("B", cache);
  getKey("B", cache);
  getKey("C", cache);
  getKey("C", cache);
  getKey("C", cache);
  cache.Put("E", "This is E");
  getKey("E", cache);

  // Should evict 'D'
  cache.Put("F", "This is F");
  getKey("F", cache);
  getKey("F", cache);
  getKey("F", cache);

  // should evict 'E'
  cache.Put("G", "This is G");
  getKey("G", cache);
  getKey("G", cache);
  getKey("G", cache);

  getKey("B", cache);

  // should evict 'A'
  cache.Put("H", "This is H");
  getKey("H", cache);

  return 0;
}
