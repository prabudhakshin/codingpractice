#include <stdio.h>

char* c_strtok(char* input, char delim) {
  static char* src = input;
  static int start_pos = 0;
  static int end_pos = -1;

  start_pos = end_pos+1;

  while (src[start_pos]== delim &&
         src[start_pos] != '\0') {
    start_pos++;
  }

  if (src[start_pos] == '\0') {
    return NULL;
  }

  end_pos = start_pos+1;
  while(src[end_pos] != delim &&
        src[end_pos] != '\0') {
    end_pos++;
  }

  if (src[end_pos] == '\0') {
    end_pos--;
    return &src[start_pos];
  }

  src[end_pos] = '\0';
  return &src[start_pos];
}

int main() {
  char str[] ="This a    sample   string ";
  char * pch;
  printf ("Splitting string \"%s\" into tokens:\n",str);
  pch = c_strtok (str,' ');
  while (pch != NULL)
  {
    printf ("%s\n",pch);
    pch = c_strtok (NULL, ' ');
  }
  return 0;
}
