#include<iostream>
#include <list>
#include <string>
using namespace std;

list<string> GetCombi(string ip) {
  if (ip.size() == 1) {
    list<string> combi;
    combi.push_back("");
    combi.push_back(ip);
    return combi;
  }

  char firstChar = ip[0];
  string remaining = ip.substr(1);
  list<string> subcombis = GetCombi(remaining);
  list<string> combis(subcombis);

  for (string s : subcombis) {
    combis.push_back(firstChar + s);
  }

  return combis;
}

int main() {
  list<string> combis = GetCombi("prabu");
  for (string s : combis) {
    cout << s << "\n";
  }
  return 0;
}
