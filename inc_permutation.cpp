// Given an array of n integers, find the n! permutations
// of the numbers such that they are in the increasing order
// of the value.

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void inc_permute(vector<int> ip, int startpos=0) {
  if (startpos == ip.size()-1) {
    for (int num : ip) {
      cout << num << " ";
    }
    cout << "\n";
  }

  vector<int> sorted_ip(ip);
  sort(sorted_ip.begin()+startpos, sorted_ip.end());

  for (int i=startpos; i<sorted_ip.size(); i++) {
    vector<int> temp = sorted_ip;
    //swap startpos and i
    int t = temp[startpos];
    temp[startpos] = temp[i];
    temp[i] = t;

    inc_permute(temp, startpos+1);
  }
}

int main() {
  vector<int> ip = {3,1,4,2};
  inc_permute(ip);
  return 0;
}
